extern crate rand;

use rand::Rng;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::time::{SystemTime};

/// Fontset sprites, loaded into memory at 0x000-0x04F.
static FONTSET: &'static [u8] = &[
    0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80, //F
];

/// Initial program counter.
static INITIAL_PC: u16 = 0x0200;

/// Nanoseconds per CPU instruction.
static CYCLE_TIME: u128 = 1700000;

/// Trait for Chip8 IO device. Must implement get_key and
/// write_display. Optionally, can implement buzzer.
pub trait Chip8IO {
    fn get_key(&mut self) -> [bool; 0x10];
    fn write_display(&mut self, _: &[u8; 0x800]);
    fn buzzer(&mut self, _: bool) {}
}

/// Chip8 virtual hardware.
pub struct Chip8<'a> {
    /// RAM memory (4096 bytes)
    pub mem: [u8; 0x1000],
    /// Stack (16 16-bit words)
    pub stk: [u16; 0x10],
    /// Display memory (64x32 monochromatic)
    pub dsp: [u8; 0x800],
    /// 16 8-bit registers
    pub v: [u8; 0x10],
    /// Key press states
    pub key: [bool; 0x10],
    /// 16-bit address register
    pub i: u16,
    /// Delay timer
    pub dt: u8,
    /// Sound timer
    pub st: u8,
    /// Program counter
    pub pc: u16,
    /// Stack pointer
    pub sp: u16,
    /// IO device
    pub io: &'a mut (dyn Chip8IO + 'a),
    /// Opcode dispatch table
    dispatch: HashMap<u16, fn(&mut Chip8, u16)>,
}

impl<'a> Chip8<'a> {
    /// Create new Chip8 device, using given IO device.
    pub fn new(io: &'a mut dyn Chip8IO) -> Chip8<'a> {
        let mut m = [0; 0x1000];
        m[..FONTSET.len()].clone_from_slice(FONTSET);
        Chip8 {
            mem: m,
            stk: [0; 0x10],
            dsp: [0; 0x800],
            v: [0; 0x10],
            key: [false; 0x10],
            i: 0,
            dt: 0,
            st: 0,
            pc: INITIAL_PC,
            sp: 0,
            io: io,
            dispatch: gen_dispatch(),
        }
    }
    /// Load ROM program file into memory, starting at inital program counter position.
    pub fn load_program(&mut self, filename: &str) -> std::io::Result<()> {
        let mut file = File::open(filename)?;
        file.read(&mut self.mem[INITIAL_PC as usize..])?;
        Ok(())
    }
    /// Execute the next Chip8 instruction.
    pub fn execute(&mut self) {
        let mut opcode: u16 = (self.mem[self.pc as usize] as u16) << 8;
        opcode |= self.mem[(self.pc + 1) as usize] as u16;

        let base = decode(opcode);

        match self.dispatch.get(&base) {
            Some(f) => f(self, opcode),
            None => {}
        };
    }
    /// Initiate CPU loop.
    pub fn run(&mut self) {
        let mut t0 = SystemTime::now();
        let mut t0_60_hz = t0;

        loop {
            let t1 = SystemTime::now();

            self.key = self.io.get_key();

            // execute instruction every CYCLE_TIME nanoseconds
            if t1.duration_since(t0).expect("System time error").as_nanos() >= CYCLE_TIME {
                self.execute();
                t0 = t1;
            } else {
                continue
            }

            //decrement DT/ST at 60 Hz
            if t1.duration_since(t0_60_hz).expect("System time error").as_nanos() >= 16666667 {
                match self.dt {
                    0 => {},
                    _ => {self.dt -= 1},
                }
                match self.st {
                    0 => self.io.buzzer(false),
                    _ => {
                        self.st -= 1;
                        self.io.buzzer(true);
                    }
                }
                t0_60_hz = t1;
            }
        }
    }
}

/// Decode full opcode word into base opcode for dispatch table.
fn decode(opcode: u16) -> u16 {
    match 0xF000 & opcode {
        0x0000 => opcode,
        0x5000 => {
            if opcode & 0x000F == 0x0000 {
                0x5000
            } else {
                opcode
            }
        }
        0x8000 => opcode & 0xF00F,
        0x9000 => {
            if opcode & 0x000F == 0x0000 {
                0x9000
            } else {
                opcode
            }
        }
        0xE000 => opcode & 0xF0FF,
        0xF000 => opcode & 0xF0FF,
        _ => 0xF000 & opcode,
    }
}

/// Generate dispatch table hashmap.
fn gen_dispatch() -> HashMap<u16, fn(&mut Chip8, u16)> {
    let mut map = HashMap::new();
    map.insert(0x00E0, CLS);
    map.insert(0x00EE, RET);
    map.insert(0x1000, JPM);
    map.insert(0x2000, CALL);
    map.insert(0x3000, SEVN);
    map.insert(0x4000, SNEVN);
    map.insert(0x5000, SEVV);
    map.insert(0x6000, LDVN);
    map.insert(0x7000, ADDVN);
    map.insert(0x8000, LDVV);
    map.insert(0x8001, OR);
    map.insert(0x8002, AND);
    map.insert(0x8003, XOR);
    map.insert(0x8004, ADDVV);
    map.insert(0x8005, SUBVV);
    map.insert(0x8006, SHR);
    map.insert(0x8007, SUBNVV);
    map.insert(0x800E, SHL);
    map.insert(0x9000, SNEVV);
    map.insert(0xA000, LDIM);
    map.insert(0xB000, JPV);
    map.insert(0xC000, RND);
    map.insert(0xD000, DRAW);
    map.insert(0xE09E, SKP);
    map.insert(0xE0A1, SKPN);
    map.insert(0xF007, LDVD);
    map.insert(0xF00A, LDVK);
    map.insert(0xF015, LDDV);
    map.insert(0xF018, LDSV);
    map.insert(0xF01E, ADDI);
    map.insert(0xF029, LDSP);
    map.insert(0xF033, LDBCD);
    map.insert(0xF055, LDMV);
    map.insert(0xF065, LDVM);
    map
}

//Opcode implementations

static CLS: fn(&mut Chip8, u16) = |c, _| {
    c.dsp = [0; 0x800];
    c.pc += 2;
    c.io.write_display(&c.dsp);
};
static RET: fn(&mut Chip8, u16) = |c, _| {
    c.pc = c.stk[c.sp as usize] + 2;
    c.sp -= 1;
};
static JPM: fn(&mut Chip8, u16) = |c, o| {
    c.pc = o & 0x0FFF;
};
static CALL: fn(&mut Chip8, u16) = |c, o| {
    c.sp += 1;
    c.stk[c.sp as usize] = c.pc;
    c.pc = o & 0x0FFF;
};
static SEVN: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let n = (o & 0x00FF) as u8;
    if c.v[x] == n {
        c.pc += 4
    } else {
        c.pc += 2
    };
};
static SNEVN: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let n = (o & 0x00FF) as u8;
    if c.v[x] != n {
        c.pc += 4
    } else {
        c.pc += 2
    };
};
static SEVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    if c.v[x] == c.v[y] {
        c.pc += 4
    } else {
        c.pc += 2
    };
};
static LDVN: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let n = (o & 0x00FF) as u8;
    c.v[x] = n;
    c.pc += 2;
};
static ADDVN: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let n = (o & 0x00FF) as u8;
    c.v[x] += n;
    c.pc += 2;
};
static LDVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[x] = c.v[y];
    c.pc += 2;
};
static OR: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[x] = c.v[x] | c.v[y];
    c.pc += 2;
};
static AND: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[x] = c.v[x] & c.v[y];
    c.pc += 2;
};
static XOR: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[x] = c.v[x] ^ c.v[y];
    c.pc += 2;
};
static ADDVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[0xF] = if 0xFF - c.v[x] < c.v[y] { 0x01 } else { 0x00 };
    c.v[x] += c.v[y];
    c.pc += 2;
};
static SUBVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[0xF] = if c.v[x] > c.v[y] { 0x01 } else { 0x00 };
    c.v[x] -= c.v[y];
    c.pc += 2;
};
static SHR: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.v[0xF] = c.v[x] & 0x01;
    c.v[x] >>= 1;
    c.pc += 2;
};
static SUBNVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    c.v[0xF] = if c.v[y] > c.v[x] { 0x01 } else { 0x00 };
    c.v[y] -= c.v[x];
    c.pc += 2;
};
static SHL: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.v[0xF] = c.v[x] & 0x80;
    c.v[x] <<= 1;
    c.pc += 2;
};
static SNEVV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    if c.v[x] != c.v[y] {
        c.pc += 4
    } else {
        c.pc += 2
    };
};
static LDIM: fn(&mut Chip8, u16) = |c, o| {
    c.i = o & 0x0FFF;
    c.pc += 2;
};
static JPV: fn(&mut Chip8, u16) = |c, o| {
    c.pc = (o & 0x0FFF) + (c.v[0x0] as u16);
    //add 2 offset?
};
static RND: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let n = (o & 0x00FF) as u8;
    let r = rand::thread_rng().gen::<u8>();
    c.v[x] = r & n;
    c.pc += 2;
};
static DRAW: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let y = ((o & 0x00F0) >> 4) as usize;
    let n = (o & 0x000F) as u8;
    let mut m = c.i as usize;

    c.v[0xF] = 0x0; //reset VF register

    //iterate over height of sprite
    for i in 0u8..n {
        let dy = (c.v[y] + i) % 32; //pixel y w/ wrapping
                                    //iterate over width of sprite (8 pixels)
        for j in 0u8..8 {
            let dx = (c.v[x] + j) % 64; //pixel y w/ wrapping
            let bit = (c.mem[m] & (0x80 >> j)) >> (0x7 - j); //get bit
            let vptr = (dx as usize) + 64 * (dy as usize);
            if bit & c.dsp[vptr] == 0x01 {
                c.v[0xF] = 0x01 //set VF if pixel overwritten
            }
            c.dsp[vptr] = bit ^ c.dsp[vptr];
        }
        m += 1;
    }
    c.pc += 2;
    c.io.write_display(&c.dsp);
};
static SKP: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    if c.key[c.v[x] as usize] {
        c.pc += 4
    } else {
        c.pc += 2
    }
};
static SKPN: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    if !c.key[c.v[x] as usize] {
        c.pc += 4
    } else {
        c.pc += 2
    }
};
static LDVD: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.v[x] = c.dt;
    c.pc += 2;
};
static LDVK: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    'l: loop {
        c.key = c.io.get_key();
        for i in 0..0x10 {
            if c.key[i] {
                c.v[x] = i as u8;
                break 'l;
            }
        }
    }
    c.pc += 2;
};
static LDDV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.dt = c.v[x];
    c.pc += 2;
};
static LDSV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.st = c.v[x];
    c.pc += 2;
};
static ADDI: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.i += c.v[x] as u16;
    c.pc += 2;
};
static LDSP: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    c.i = (c.v[x] as u16) * 5;
    c.pc += 2;
};
static LDBCD: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    let ptr = c.i as usize;
    c.mem[ptr] = c.v[x] / 100;
    c.mem[ptr + 1] = (c.v[x] % 100) / 10;
    c.mem[ptr + 2] = c.v[x] % 10;
    c.pc += 2;
};
static LDMV: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    for i in 0..=x {
        c.mem[c.i as usize + i] = c.v[i];
    }
    c.pc += 2;
};
static LDVM: fn(&mut Chip8, u16) = |c, o| {
    let x = ((o & 0x0F00) >> 8) as usize;
    for i in 0..=x {
        c.v[i] = c.mem[c.i as usize + i];
    }
    c.pc += 2;
};
