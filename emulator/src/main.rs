extern crate sdl2;

mod emulator;

use sdl2::audio::{AudioCallback, AudioSpecDesired};
use sdl2::event::Event;
use sdl2::keyboard::Scancode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

/// Width of display in pixels.
static WIDTH: u32 = 64;
/// Height of display in pixels.
static HEIGHT: u32 = 32;
/// Scaling factor for display pixels.
static SCALE: u32 = 16;

/// Display foreground color.
static FG: Color = Color::RGB(127, 150, 150);
/// Display background color.
static BG: Color = Color::RGB(0, 0, 0);

/// Square wave for buzzer.
struct SquareWave {
    phase_inc: f32,
    phase: f32,
    volume: f32,
}

/// SDL-based IO device for Chip8
struct SdlIO {
    canvas: sdl2::render::Canvas<sdl2::video::Window>,
    events: sdl2::EventPump,
    audio: sdl2::audio::AudioDevice<SquareWave>,
}

impl AudioCallback for SquareWave {
    type Channel = f32;
    fn callback(&mut self, out: &mut [f32]) {
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                self.volume
            } else {
                -self.volume
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

impl emulator::Chip8IO for SdlIO {
    fn get_key(&mut self) -> [bool; 0x10] {
        let mut a = [false; 0x10];
        for e in self.events.poll_iter() {
            match e {
                Event::Quit { .. } => {
                    std::process::exit(1);
                }
                _ => {}
            }
        }
        for k in self.events.keyboard_state().pressed_scancodes() {
            match k {
                Scancode::Num1 => {
                    a[0x1] = true;
                }
                Scancode::Num2 => {
                    a[0x2] = true;
                }
                Scancode::Num3 => {
                    a[0x3] = true;
                }
                Scancode::Num4 => {
                    a[0xC] = true;
                }
                Scancode::Q => {
                    a[0x4] = true;
                }
                Scancode::W => {
                    a[0x5] = true;
                }
                Scancode::E => {
                    a[0x6] = true;
                }
                Scancode::R => {
                    a[0xD] = true;
                }
                Scancode::A => {
                    a[0x7] = true;
                }
                Scancode::S => {
                    a[0x8] = true;
                }
                Scancode::D => {
                    a[0x9] = true;
                }
                Scancode::F => {
                    a[0xE] = true;
                }
                Scancode::Z => {
                    a[0xA] = true;
                }
                Scancode::X => {
                    a[0x0] = true;
                }
                Scancode::C => {
                    a[0xB] = true;
                }
                Scancode::V => {
                    a[0xF] = true;
                }
                _ => {}
            }
        }
        a
    }
    fn write_display(&mut self, buffer: &[u8; 0x800]) {
        self.canvas.set_draw_color(BG);
        self.canvas.clear();

        let mut r = Vec::new();
        for w in 0..WIDTH {
            for h in 0..HEIGHT {
                let inx = (w + WIDTH * h) as usize;
                if buffer[inx] > 0 {
                    r.push(Rect::new(
                        (w * SCALE) as i32,
                        (h * SCALE) as i32,
                        SCALE,
                        SCALE,
                    ))
                }
            }
        }
        self.canvas.set_draw_color(FG);
        self.canvas.fill_rects(&r).unwrap();

        self.canvas.present();
    }
    fn buzzer(&mut self, on: bool) {
        if on {
            self.audio.resume()
        } else {
            self.audio.pause()
        }
    }
}

impl SdlIO {
    /// Create and initialize new SdlIO device.
    pub fn new() -> SdlIO {
        let c = sdl2::init().unwrap();
        let v = c.video().unwrap();
        let w = v
            .window("Chip8", WIDTH * SCALE, HEIGHT * SCALE)
            .position_centered()
            .build()
            .unwrap();
        let mut cv = w.into_canvas().build().unwrap();
        let ep = c.event_pump().unwrap();
        let a = c.audio().unwrap();
        let aspec = AudioSpecDesired {
            freq: Some(44100),
            channels: Some(1),
            samples: None,
        };
        let ad = a
            .open_playback(None, &aspec, |spec| SquareWave {
                phase_inc: 880.0 / spec.freq as f32,
                phase: 0.0,
                volume: 0.01,
            })
            .unwrap();

        cv.set_draw_color(BG);
        cv.clear();
        cv.present();

        SdlIO {
            canvas: cv,
            events: ep,
            audio: ad,
        }
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let mut io_device = SdlIO::new();
    let mut ch8 = emulator::Chip8::new(&mut io_device);

    if args.len() > 1 {
        ch8.load_program(&args[1]).expect("Could not load ROM");
    } else {
        println!("No ROM supplied.");
        std::process::exit(1);
    }

    ch8.run();
}
