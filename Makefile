CXX = g++
CFLAGS = --std=c++17 -Wall -Wextra -fPIC -o3

BIN_DIR = bin

ASM_TARGET  = asm
ASM_SRC_DIR = assembler
ASM_OBJ_DIR = build/asm
ASM_SRC     = $(wildcard $(ASM_SRC_DIR)/*.cpp)
ASM_OBJ     = $(ASM_SRC:$(ASM_SRC_DIR)/%.cpp=$(ASM_OBJ_DIR)/%.o)
ASM_INCLUDES = -Iinclude

all: $(ASM_TARGET)

$(ASM_TARGET): $(ASM_OBJ)
	mkdir -p $(BIN_DIR)
	$(CXX) $(CFLAGS) $(ASM_OBJ) -o $(ASM_TARGET)
$(ASM_OBJ_DIR)/%.o: $(ASM_SRC_DIR)/%.cpp
	mkdir -p $(ASM_OBJ_DIR)
	$(CXX) $(CFLAGS) $(ASM_INCLUDES) -c $< -o $@

clean:
	rm -rf build/
	rm $(ASM_TARGET)
	rm -rf $(BIN_DIR)

asmtests: $(ASM_TARGET)
	./$(ASM_TARGET) -i test/loop_char.s -o ch8bin/loop_char.ch8
