//Based on cowgod chip8 reference

#include "ch8utils/ansi_escape.hpp"
#include "ch8utils/nums.hpp"

#include "parser.hpp"

using namespace c8asm::tokenizer;

using std::ostringstream;
using std::size_t;
using std::string;
using std::vector;
using std::unordered_map;

#define INST(val) {#val, InstPair{val, _##val}}

#define SHIFT_2_NIBBLE(val) (val << 8)
#define SHIFT_1_NIBBLE(val) (val << 4)

namespace c8asm::parser
{
    // Check if Token is VREG (V[0-F]).
    bool is_vreg(Token t)
    {
        auto v = t.value;
        if (t.id != IDENTIFIER) return false;
        return (
            v == "V0" ||
            v == "V1" ||
            v == "V2" ||
            v == "V3" ||
            v == "V4" ||
            v == "V5" ||
            v == "V6" ||
            v == "V7" ||
            v == "V8" ||
            v == "V9" ||
            v == "VA" ||
            v == "VB" ||
            v == "VC" ||
            v == "VD" ||
            v == "VE" ||
            v == "VF"
        );
    }

    // Check if Token is I Register.
    bool is_ireg(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "I";
    }

    // Check if Token is DT.
    bool is_dt(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "DT";
    }

    // Check if Token is ST.
    bool is_st(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "ST";
    }

    // Check if Token is F.
    bool is_f(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "F";
    }

    // Check if Token is K.
    bool is_k(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "K";
    }

    // Check if Token is B.
    bool is_b(Token t)
    {
        if (t.id != IDENTIFIER) return false;
        return t.value == "B";
    }

    // Check if Token is numerical literal.
    bool is_numerical(Token t)
    {
        return (t.id == NUM_LITERAL);
    }

    // Check if Token is label to memory.
    bool is_mem_ref(Token t)
    {
        return (t.id == LABEL && !is_vreg(t) && !is_ireg(t));
    }

    // Convert VREG (V[0-F]) to integer.
    uint16_t vreg_to_int(string vreg)
    {
        string substr = vreg.substr(1,vreg.size()-1);
        return utils::num::hex_to_int(substr);
    }

    // If argument count is not == expected_length, generate parser error.
    void check_arg_length_equals(Parser &p, TokenList &args, string ins, size_t expected_length)
    {
        if (args.size() != expected_length)
        {
            ostringstream str;
            str << ins << ": invalid argument count: expected " << expected_length << " , got " << args.size();
            p.error(str.str());
        }
        return;
    }

    // If argument count is not > expected_length, generate parser error.
    void check_arg_length_greater_than(Parser &p, TokenList &args, string ins, size_t expected_length)
    {
        if (args.size() <= expected_length)
        {
            ostringstream str;
            str << ins << ": invalid argument count: expected >" << expected_length << " , got " << args.size();
            p.error(str.str());
        }
        return;
    }

    // Generates unsupported arguments error in parser.
    void error_unsupported_args(Parser &p, TokenList &args, string ins)
    {
        ostringstream str;
        str << "invalid arguments to instruction " << ins << " (";
        for (size_t i = 0; i < args.size(); i++)
        {
            str << args[i].value;
            if (i < args.size()-1) str << ",";
        }
        str << ")";

        p.error(str.str());
        return;
    }

    // Instructions
    void _ADD(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"ADD",2);
        if (is_vreg(args[0]) && is_numerical(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t byte = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0x7000 | SHIFT_2_NIBBLE(reg) | byte;
            p.program.push_back(ASM_OP{
                .id=OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8004 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id=OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_ireg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF01E | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id=OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"ADD");
    }
    void _AND(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"AND",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8002 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"AND");
    }
    void _CALL(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"CALL",1);
        if (is_numerical(args[0]))
        {
            uint16_t addr = utils::num::string_to_int(args[0].value);
            uint16_t opcode = 0x2000 | addr;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_mem_ref(args[0]))
        {
            uint16_t opcode = 0x2000;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
                .mem_symbol = args[0].value,
            });
            return;
        }
        error_unsupported_args(p,args,"CALL");
    }
    void _CLS(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"CLS",0);
        p.program.push_back(ASM_OP{
            .id = OPCODE,
            .opcode = 0x00E0,
        });
    }
    void _DRW(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"DRW",3);
        if (is_vreg(args[0]) && is_vreg(args[1]) && is_numerical(args[2]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t nib = utils::num::string_to_int(args[2].value);
            uint16_t opcode = 0xD000 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2) | nib;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }

        error_unsupported_args(p,args,"DRW");
    }
    void _JP(Parser &p, TokenList &args)
    {
        check_arg_length_greater_than(p,args,"JP",0);
        if (is_numerical(args[0]))
        {
            check_arg_length_equals(p,args,"JP",1);
            uint16_t addr = utils::num::string_to_int(args[0].value);
            uint16_t opcode = 0x1000 | addr;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_mem_ref(args[0]))
        {
            check_arg_length_equals(p,args,"JP",1);
            uint16_t opcode = 0x1000;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
                .mem_symbol = args[0].value,
            });
            return;
        }
        check_arg_length_equals(p,args,"JP",2);
        if (is_vreg(args[0]) && is_numerical(args[1]) && args[0].value == "V0")
        {
            uint16_t addr = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0xB000 | addr;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_mem_ref(args[1]) && args[0].value == "V0")
        {
            uint16_t opcode = 0xB000;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
                .mem_symbol = args[1].value,
            });
        }
        error_unsupported_args(p,args,"JP");
    }
    void _LD(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"LD",2);
        if (is_vreg(args[0]) && is_numerical(args[1]))
        {
            uint16_t byte = utils::num::string_to_int(args[1].value);
            uint16_t reg  = vreg_to_int(args[0].value);
            uint16_t opcode = 0x6000 | SHIFT_2_NIBBLE(reg) | byte;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8000 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_ireg(args[0]) && is_numerical(args[1]))
        {
            uint16_t addr = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0xA000 | addr;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_ireg(args[0]) && is_mem_ref(args[1]))
        {
            uint16_t opcode = 0xA000;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
                .mem_symbol = args[1].value,
            });
            return;
        }
        if (is_vreg(args[0]) && is_dt(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0xF007 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_dt(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF015 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_st(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF018 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_ireg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF055 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_ireg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0xF065 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_k(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0xF00A | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_f(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF029 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_b(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg = vreg_to_int(args[1].value);
            uint16_t opcode = 0xF033 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"LD");
    }
    void _OR(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"OR",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8001 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"OR");
    }
    void _RET(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"RET",0);
        p.program.push_back(ASM_OP{
            .id = OPCODE,
            .opcode = 0x00EE,
        });
    }
    void _RND(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"RND",2);
        if (is_vreg(args[0]) && is_numerical(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t byte = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0xC000 | SHIFT_2_NIBBLE(reg) | byte;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"RND");
    }
    void _SE(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SE",2);
        if (is_vreg(args[0]) && is_numerical(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t byte = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0x3000 | SHIFT_2_NIBBLE(reg) | byte;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x5000 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SE");
    }
    void _SHL(Parser &p, TokenList &args)
    {
        check_arg_length_greater_than(p,args,"SHL",0);
        if (is_vreg(args[0]) && args.size() == 1)
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0x800E | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        check_arg_length_equals(p,args,"SHL",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x800E | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SHL");
    }
    void _SHR(Parser &p, TokenList &args)
    {
        check_arg_length_greater_than(p,args,"SHR",0);
        if (is_vreg(args[0]) && args.size() == 1)
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0x8006 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        check_arg_length_equals(p,args,"SHR",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8006 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SHR");
    }
    void _SKNP(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SKNP",1);
        if (is_vreg(args[0]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0xE0A1 | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SKNP");
    }
    void _SKP(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SKP",1);
        if (is_vreg(args[0]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t opcode = 0xE09E | SHIFT_2_NIBBLE(reg);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SKP");
    }
    void _SNE(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SNE",2);
        if (is_vreg(args[0]) && is_numerical(args[1]))
        {
            uint16_t reg = vreg_to_int(args[0].value);
            uint16_t byte = utils::num::string_to_int(args[1].value);
            uint16_t opcode = 0x4000 | SHIFT_2_NIBBLE(reg) | byte;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x5000 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SNE");
    }
    void _SUB(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SUB",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8005 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }

        error_unsupported_args(p,args,"SUB");
    }
    void _SUBN(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SUBN", 2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8007 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"SUBN");
    }
    void _SYS(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"SYS",1);
        if (is_numerical(args[0]))
        {
            uint16_t addr = utils::num::string_to_int(args[0].value);
            uint16_t opcode = 0x0000 | addr;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        if (is_mem_ref(args[0]))
        {
            uint16_t opcode = 0x0000;
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
                .mem_symbol = args[0].value,
            });
            return;
        }
        error_unsupported_args(p,args,"SYS");
    }
    void _XOR(Parser &p, TokenList &args)
    {
        check_arg_length_equals(p,args,"XOR",2);
        if (is_vreg(args[0]) && is_vreg(args[1]))
        {
            uint16_t reg1 = vreg_to_int(args[0].value);
            uint16_t reg2 = vreg_to_int(args[1].value);
            uint16_t opcode = 0x8003 | SHIFT_2_NIBBLE(reg1) | SHIFT_1_NIBBLE(reg2);
            p.program.push_back(ASM_OP{
                .id = OPCODE,
                .opcode = opcode,
            });
            return;
        }
        error_unsupported_args(p,args,"XOR");
    }

    // Utility functions

    bool Parser::is_instruction()
    {
        if (this->instruction_map.count(this->current_token().value))
            return true;
        return false;
    }
    void Parser::instruction()
    {
        auto t = this->current_token();
        // Extract arguments
        TokenList args = TokenList{};
        this->goto_next_token();
        while (!(this->current_token().id & (ENDL | NULL_TOKEN)))
        {
            if (this->current_token().id & (IDENTIFIER | LABEL | NUM_LITERAL))
                args.push_back(this->current_token());
            this->goto_next_token();
        }
        // Call instruction building function
        (this->instruction_map)[t.value].second(*this, args);
    }

    void Parser::label_declaration()
    {
        this->program.push_back(ASM_OP{
            .id=LABEL_DEF,
            .str_argument=this->current_token().value,
        });
    }

    void Parser::handle_token()
    {
        switch (this->current_token().id)
        {
        case IDENTIFIER:
            if (this->is_instruction()) this->instruction();
            else this->error("unrecognized identifier");
            break;
        case SECTION_LABEL:
            this->error("section labels unsupported");
            break;
        case LABEL:
            //this case only reached on valid declaration. if no colon, invalid.
            if (this->peek_token(1).id != COLON) this->error("Invalid reference to section");
            this->label_declaration();
            break;
        //unreachable
        default:
            break;
        }
    }

    // Public

    Parser::Parser(tokenizer::TokenList const *tokens)
    {
        this->tokens = tokens;
        this->has_error = false;
        this->instruction_map = unordered_map<string, InstPair>{};
        this->instruction_map = {
            INST(ADD), INST(AND), INST(CALL), INST(CLS), INST(DRW),
            INST(JP), INST(LD), INST(OR),
            INST(RET), INST(RND), INST(SE), INST(SHL), INST(SHR),
            INST(SKNP), INST(SKP), INST(SNE), INST(SUB), INST(SUBN),
            INST(SYS), INST(XOR),
        };
        this->current_token_inx = 0;
        this->program = Program{};
    }

    void Parser::parse_tokens()
    {
        while (this->current_token().id != NULL_TOKEN)
        {
            if (this->current_token().id & (IDENTIFIER | SECTION_LABEL | LABEL))
            {
                this->handle_token();
            }
            this->goto_next_token();
        }
    }

    // Get value of current token (at current_token_inx)
    Token Parser::current_token()
    {
        if ((this->current_token_inx) < (*(this->tokens)).size())
            return (*(this->tokens))[this->current_token_inx];
        return Token{.id=NULL_TOKEN, .value="EOF"};
    }

    // Get value of token at position (current_token_inx + offset)
    Token Parser::peek_token(size_t offset)
    {
        if ((this->current_token_inx) + offset < (*(this->tokens)).size())
            return (*(this->tokens))[(this->current_token_inx) + offset];
        return Token{.id=NULL_TOKEN, .value="EOF"};
    }

    // Increment current_token_inx by 1
    void Parser::goto_next_token()
    {
        this->current_token_inx++;
    }

    void Parser::error(string msg)
    {
        std::cerr << ANSI_BRIGHT_RED_FG << "Parse error" << ANSI_CLEAR_COLOR << ": " << msg << std::endl;
        this->has_error = true;
    }

    // Other

    // Prints program with each OPCODE on a newline to STDERR.
    void print_program(Program &p)
    {
        for (size_t i = 0; i < p.size(); i++)
        {
            auto op = p[i];
            if (op.id == OPCODE)
            {
                std::cerr << std::hex << std::setfill('0') << std::setw(4);
                std::cerr << op.opcode;
                if (op.mem_symbol != "")
                {
                    std::cerr << " MEM SYMBOL: " << op.mem_symbol;
                }
                std::cerr << std::endl;
            }
            if (op.id == LABEL_DEF)
            {
                std::cerr << ANSI_BRIGHT_GREEN_FG;
                std::cerr << "DEFINE MEM SYMBOL: " << op.str_argument;
                std::cerr << ANSI_CLEAR_COLOR << std::endl;
            }
        }
    }
}