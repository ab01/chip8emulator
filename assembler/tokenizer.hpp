#pragma once

#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>
#include <iostream>
#include <regex>
#include <iomanip>

#include "ch8utils/ansi_escape.hpp"

namespace c8asm::tokenizer
{
    typedef enum TokenID: uint8_t
    {
        NULL_TOKEN    = 0, // ID for unrecognizable token or whitespace.
        IDENTIFIER    = 0b00000001,     // Alphanumeric token. May contain, but not begin with, (0-9) or (_).
        SECTION_LABEL = 0b00000010,  // Label for section. Must begin with period, followed by valid identifier.
        LABEL         = 0b00000100,          // Identifier for memory labels. Must begin with (_).
        NUM_LITERAL   = 0b00001000,    // Decimal, hexidecimal, or binary integer literal.
        COLON         = 0b00010000,          // Colon character (:).
        COMMA         = 0b00100000,          // Comma character (,).
        COMMENT       = 0b01000000,        // Comment token defined by semicolon (;) and all following characters on line.
        ENDL          = 0b10000000,           // END OF LINE character (\n).
    } TokenID;

    typedef struct Token
    {
        TokenID id;
        std::string value;
    } Token;

    typedef std::vector<Token> TokenList;

    // Parses lines of input into vector of Tokens.
    // Assumes input sanitized to uppercase prior.
    TokenList* process_lines(std::vector<std::string> *lines);

    // Filter NULL_TOKENS from token list. Invalidates old vector pointer.
    TokenList* filter_null(TokenList* token_list);

    // Prints syntax-highlighted tokens to STDERR.
    void output_tokens_to_stderr(TokenList *token_list);
}