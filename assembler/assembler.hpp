#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <unordered_map>

#include "parser.hpp"

namespace c8asm::assembler
{
    using namespace c8asm;

    typedef std::vector<uint16_t> BinaryProgram;

    typedef std::unordered_map<std::string, uint16_t> SymbolMap;

    SymbolMap resolve_symbols(parser::Program &program, uint16_t program_mem_start);
    parser::Program resolve_program(parser::Program &p, SymbolMap &s);
    BinaryProgram assemble(parser::Program &p);

    void save_to_file(BinaryProgram &b, std::string filename);

    void print_symbols(SymbolMap &map);
}