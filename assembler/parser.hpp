#pragma once

#include <iostream>
#include <sstream>
#include <cstdint>
#include <cstddef>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>

#include "tokenizer.hpp"


/*
Ch8 ASM based on
CowGod's CHIP8 Reference (http://devernay.free.fr/hacks/chip8/C8TECH10.HTM)
 3.1 - Standard Chip-8 Instructions
00E0 - CLS
00EE - RET
0nnn - SYS addr  ****

1nnn - JP addr  ****
Bnnn - JP V0, addr  ****

2nnn - CALL addr  ****

3xkk - SE Vx, byte
5xy0 - SE Vx, Vy

4xkk - SNE Vx, byte
9xy0 - SNE Vx, Vy

6xkk - LD Vx, byte
8xy0 - LD Vx, Vy
Annn - LD I, addr  ****
Fx07 - LD Vx, DT
Fx0A - LD Vx, K  // LDK instruction???????
Fx15 - LD DT, Vx
Fx18 - LD ST, Vx
Fx55 - LD [I], Vx
Fx65 - LD Vx, [I]
Fx29 - LD F, Vx #LDF
Fx33 - LD B, Vx #LDB

7xkk - ADD Vx, byte
8xy4 - ADD Vx, Vy
Fx1E - ADD I, Vx

8xy1 - OR Vx, Vy
8xy2 - AND Vx, Vy
8xy3 - XOR Vx, Vy
8xy5 - SUB Vx, Vy
8xy6 - SHR Vx {, Vy}
8xy7 - SUBN Vx, Vy
8xyE - SHL Vx {, Vy}

Cxkk - RND Vx, byte
Dxyn - DRW Vx, Vy, nibble
Ex9E - SKP Vx
ExA1 - SKNP Vx

Unique instructions:
ADD,
AND,
CALL,
CLS,
DRW,
JP,
LD,
OR,
RET,
RND,
SE,
SHL,
SHR,
SKNP,
SKP,
SNE,
SUB,
SUBN,
SYS,
XOR,
*/

namespace c8asm::parser
{
    typedef enum Instruction {
        ADD, AND, CALL, CLS, DRW,
        JP, LD, OR, RET,
        RND, SE, SHL, SHR, SKNP, SKP,
        SNE, SUB, SUBN, SYS, XOR,
    } Instruction;

    typedef enum ASM_OP_ID: uint8_t {
        OPCODE    = 0b0000'0001,
        LABEL_DEF = 0b0000'0010,
    } ASM_OP_ID;

    typedef struct ASM_OP {
        ASM_OP_ID id;
        uint16_t opcode;
        std::string mem_symbol;
        std::string str_argument;
    } ASM_OP;

    typedef std::vector<ASM_OP> Program;

    typedef class Parser {
        private:
            tokenizer::TokenList const *tokens;
            std::unordered_map<std::string, std::pair<Instruction, std::function<void(Parser&, tokenizer::TokenList&)>>> instruction_map;
            std::size_t current_token_inx;

            //Utility
            void handle_token();
            bool is_instruction();
            void instruction();
            void label_declaration();
        public:
            Program program;
            bool has_error;

            Parser(tokenizer::TokenList const *tokens);
            void parse_tokens();
            tokenizer::Token current_token();
            tokenizer::Token peek_token(std::size_t offset);
            void goto_next_token();
            void error(std::string msg);
    } Parser;

    void print_program(Program &p);

    typedef std::pair<Instruction, std::function<void(Parser&, tokenizer::TokenList&)>> InstPair;
}