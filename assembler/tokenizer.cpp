#include "tokenizer.hpp"

using std::string;
using std::size_t;
using std::vector;
using std::regex;

namespace c8asm::tokenizer
{
    typedef struct _token_regex {
        TokenID id;
        regex   rgx;
    } _token_regex;

    const auto token_defs = vector<_token_regex>{
        _token_regex{.id = TokenID::NULL_TOKEN,         .rgx = regex("")                                                },
        _token_regex{.id = TokenID::IDENTIFIER,         .rgx = regex("[A-Z]{1}[_A-Z0-9]*")                              },
        _token_regex{.id = TokenID::SECTION_LABEL,      .rgx = regex("[\\.]{1}[_A-Z0-9]*")                              },
        _token_regex{.id = TokenID::LABEL,              .rgx = regex("[_]{1}[_A-Z0-9]*")                                },
        _token_regex{.id = TokenID::NUM_LITERAL,        .rgx = regex("((?:0B){1}[0-1]*)|((?:0X){1}[0-9A-F]*)|([0-9]*)") },
        _token_regex{.id = TokenID::COLON,              .rgx = regex(":")                                               },
        _token_regex{.id = TokenID::COMMA,              .rgx = regex("," )                                              },
        _token_regex{.id = TokenID::COMMENT,            .rgx = regex(";{1}.*")                                          },
    };

    // Lexical analyzer for single line. Returns vector of Tokens contained within. Adds ENDL to end.
    TokenList* extract_regex(string line, vector<_token_regex> token_defs)
    {
        auto output = new TokenList;
        size_t start = 0;
        size_t end = 1;

        auto match_string = [&token_defs](string s, size_t start_inx, size_t end_inx, size_t token_inx){
            return std::regex_match(
                s.substr(start_inx,end_inx-start_inx),
                token_defs[token_inx].rgx,
                std::regex_constants::match_continuous
            );
        };
        // Iterate until entire line parsed
        while (end <= line.size())
        {
            size_t n_matches = 0;
            size_t last_match_inx = 0;
            TokenID last_match = TokenID::NULL_TOKEN;

            // Iterate until unique token found
            while (n_matches != 1 && end <= line.size())
            {
                n_matches = 0;
                for (size_t i = 0; i < token_defs.size(); i++)
                {
                    if (match_string(line, start, end, i))
                    {
                        last_match = token_defs[i].id;
                        last_match_inx = i;
                        n_matches++;
                    }
                }
                // Stop loop if no token matches current substring
                if (n_matches == 0) {break;};
                // If token matches, repeat search if it can be extended at least 1 character
                if (n_matches == 1 && end < line.size() && match_string(line,start,end+1,last_match_inx)){end++; n_matches--; continue;};
            }
            // Save matched token (or NULL_TOKEN by default if no match).
            output->push_back(Token{
                    .id = last_match,
                    .value = line.substr(start, end-start),
            });
            // Update iteration indices for line, based on match.
            // If match: start at end of match next round.
            // If no match: start at next character from start next round.
            // End is reset to character after new start.
            if (n_matches == 1)
            {
                start = end;
            }
            else
            {
                start++;
            }
            end = start + 1;
        }
        output->push_back(Token{
            .id = TokenID::ENDL,
            .value = "",
        });
        return output;
    }

    TokenList* process_lines(vector<string> *lines)
    {
        auto token_list = new TokenList;

        //DEBUG:
        for (size_t i = 0; i < lines->size(); i++)
        {
            string line = (*lines)[i];
            auto t = extract_regex(line, token_defs);
            token_list->insert(token_list->end(), t->begin(), t->end());
            delete t;
        }

        return token_list;
    }

    TokenList* filter_null(TokenList* token_list)
    {
        auto filtered = new TokenList;

        for (size_t i = 0; i < token_list->size(); i++)
        {
            auto t = &(*token_list)[i];
            if (t->id != TokenID::NULL_TOKEN) filtered->push_back(*t);
        }

        delete token_list;
        return filtered;
    }

    void output_tokens_to_stderr(TokenList *token_list)
    {
        for (size_t i = 0; i < token_list->size(); i++)
        {
            auto t = (*token_list)[i];
            switch (t.id){
            case TokenID::NULL_TOKEN:
                std::cerr << " ";
                break;
            case TokenID::IDENTIFIER:
                std::cerr << ANSI_BRIGHT_RED_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::SECTION_LABEL:
                std::cerr << ANSI_BRIGHT_MAGENTA_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::LABEL:
                std::cerr << ANSI_BRIGHT_GREEN_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::NUM_LITERAL:
                std::cerr << ANSI_CYAN_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::COLON:
            case TokenID::COMMA:
                std::cerr << ANSI_YELLOW_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::COMMENT:
                std::cerr << ANSI_BRIGHT_BLACK_FG << t.value << ANSI_CLEAR_COLOR;
                break;
            case TokenID::ENDL:
                std::cerr << std::endl;
                break;
            };
        }
    }
}