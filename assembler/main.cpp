#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <vector>
#include <cstddef>

#include "ch8utils/io.hpp"

#include "tokenizer.hpp"
#include "parser.hpp"
#include "assembler.hpp"

#ifndef PROGRAM_START
#define PROGRAM_START 0x0200
#endif

using namespace c8asm;

using std::string;
using std::vector;
using std::size_t;

using tokenizer::TokenList;
using parser::Parser;
using parser::Program;
using assembler::SymbolMap;
using assembler::BinaryProgram;

// Runs tokenizer, parser, and assembler on input string.
// Outputs to STDOUT.
inline void process_asm_input(vector<string> *input, string output, bool verbose)
{
    if (verbose) std::cerr << "Tokenizing ASM input." << std::endl;
    TokenList* token_list = tokenizer::process_lines(input);
    if (verbose) tokenizer::output_tokens_to_stderr(token_list);
    token_list = tokenizer::filter_null(token_list);

    if (verbose) std::cerr << "Parsing tokens." << std::endl;
    Parser parser = Parser(token_list);
    parser.parse_tokens();
    if (verbose) parser::print_program(parser.program);

    if (verbose) std::cerr << "Assembling program." << std::endl;
    SymbolMap smap = assembler::resolve_symbols(parser.program, PROGRAM_START);
    if (verbose) assembler::print_symbols(smap);
    Program pgm = assembler::resolve_program(parser.program, smap);
    if (verbose) parser::print_program(pgm);
    BinaryProgram bin = assembler::assemble(pgm);

    if (verbose) std::cerr << "Saving binary file: " << output << std::endl;

    assembler::save_to_file(bin, output);
}

inline void print_help()
{
    string text =
        "Chip8 Assembler\n"
        "Assembles Chip8ASM into bytecode.\n"
        "Usage: asm [flags]\n"
        "Flags:\n"
        "\t -h/--help: \tDisplay this help page.\n"
        "\t -i/--input <filename>: \tRead from specified file.\n"
        "\t -o/--output <filename>: \tSpecify output file.\n"
        "\t -s/--stdin: \tRead from STDIN. Alternatively use: 'asm .'\n"
        "\t -v/--verbose: \tPrint verbose output to STDERR. \n"
    ;
    std::cerr << text << std::endl;
}

int main(int argc, char** argv)
{
    vector<string>* input_string = 0; // Will check if null before dereferencing.
    bool verbose = false;
    string out_name = "a.out";

    // Check if flags/arguments provided, if not, print help.
    if (argc <= 1)
    {
        print_help();
        return EXIT_FAILURE;
    }

    // Parse arguments.
    string arg;
    for (size_t i = 1; i < (size_t)argc; i++)
    {
        arg = (string)(argv[i]);

        if (arg == "-h" or arg == "--help")
        {
            print_help();
            return EXIT_FAILURE;
        }
        if (arg == "-i" or arg == "--input")
        {
            assert((size_t)argc > i+1);
            input_string = utils::io::read_file((string)(argv[++i]));
        }
        if (arg == "-o" or arg == "--output")
        {
            assert((size_t)argc > i+1);
            out_name = (string)(argv[++i]);
        }
        if (arg == "-s" or arg == "." or arg == "--stdin")
        {
            input_string = utils::io::read_stdin();
        }
        if (arg == "-v" or arg == "--verbose")
        {
            verbose = true;
        }
    }

    // Check valid pointer to input string
    if (!input_string) return EXIT_FAILURE;

    // Run assembler.
    process_asm_input(input_string, out_name, verbose);
    return EXIT_SUCCESS;
}