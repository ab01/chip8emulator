#include "assembler.hpp"

#include "ch8utils/ansi_escape.hpp"

#ifndef WORD_SIZE
#define WORD_SIZE 2
#endif

using std::string;
using std::vector;
namespace c8asm::assembler
{
    SymbolMap resolve_symbols(parser::Program &program, uint16_t program_mem_start)
    {
        SymbolMap smap = SymbolMap{};

        uint16_t current_mem_addr = program_mem_start;

        for (size_t i = 0; i < program.size(); i++)
        {
            auto op = program[i];
            if (op.id == parser::ASM_OP_ID::OPCODE) current_mem_addr += WORD_SIZE;
            if (op.id == parser::ASM_OP_ID::LABEL_DEF)
            {
                smap.insert({op.str_argument, current_mem_addr});
            }
        }
        return smap;
    }

    void print_symbols(SymbolMap &map)
    {
        std::cerr << "Symbols: ";
        for (auto const& s: map)
        {
            std::cerr << "(";
            std::cerr << s.first << " = 0x" << std::hex << std::setfill('0') << std::setw(4) << s.second;
            std::cerr << ") ";
        }
        std::cerr << std::endl;
    }

    parser::Program resolve_program(parser::Program &p, SymbolMap &s)
    {
        auto pout = parser::Program{};

        for (size_t i = 0; i < p.size(); i++)
        {
            auto op = p[i];
            if (op.id == parser::ASM_OP_ID::OPCODE && op.mem_symbol != "")
            {
                if (s.count(op.mem_symbol))
                {
                    uint16_t opcode = op.opcode | s[op.mem_symbol];
                    pout.push_back(parser::ASM_OP{
                        .id=parser::ASM_OP_ID::OPCODE,
                        .opcode = opcode,
                    });
                }
                else
                {
                    std::cerr << ANSI_BRIGHT_RED_FG << "Error" << ANSI_CLEAR_COLOR << ": undefined symbol " << op.mem_symbol << std::endl;
                }
            }
            else if (op.id == parser::ASM_OP_ID::OPCODE)
            {
                pout.push_back(op);
            }
        }

        return pout;
    }

    BinaryProgram assemble(parser::Program &p)
    {
        auto bin = BinaryProgram{};
        for (size_t i = 0; i < p.size(); i++)
        {
            auto op = p[i];
            if (op.id != parser::ASM_OP_ID::OPCODE || op.mem_symbol != "")
            {
                std::cerr << ANSI_BRIGHT_RED_FG << "Error" << ANSI_CLEAR_COLOR << ": invalid program" << std::endl;
                return BinaryProgram{};
            }
            bin.push_back(op.opcode);
        }
        return bin;
    }

    void save_to_file(BinaryProgram &b, string filename)
    {
        std::ofstream fout;
        vector<uint8_t> bytes = {};
        for (auto word: b)
        {
            uint8_t lb = (uint8_t) ((word & 0xFF00) >> 8);
            uint8_t rb = (uint8_t) (word & 0x00FF);
            bytes.push_back(lb);
            bytes.push_back(rb);
        }
        fout.open(filename, std::ios::binary | std::ios::out);
        fout.write((char*)&(bytes[0]), bytes.size());
        fout.close();
    }
}