_start:
    ld v0, 0    ; character index
    ld v1, 0x1E   ; X position to draw (center of display)
    ld v2, 0x0D ; Y position to draw (center of display)

    _draw_loop:
    cls
    ld f, v0
    drw v1, v2, 5 ; draw character sprite (5 pixels high)
    call _delay
    add v0, 1
    sne v0, 0x10
    ld v0, 0
    jp _draw_loop

    _delay:
    ld v3, 60 ; wait 1 second
    ld dt, v3
    _decrement_timer:
    ld v3, dt
    se v3, 0
    jp _decrement_timer
    ret