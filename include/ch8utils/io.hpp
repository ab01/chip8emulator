#pragma once

#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <cctype>
#include <cstddef>

namespace utils::io
{
    using std::string;
    using std::vector;
    using std::size_t;

    // Converts string to lowercase
    inline string to_lower(string input)
    {
        char* d = input.data();
        for (size_t i = 0; i < input.size(); i++)
        {
            d[i] = (char)std::tolower((unsigned char)d[i]);
        }
        return string(d);
    }
    // Converts string to uppercase
    inline string to_upper(string input)
    {
        char* d = input.data();
        for (size_t i = 0; i < input.size(); i++)
        {
            d[i] = (char)std::toupper((unsigned char)d[i]);
        }
        return string(d);
    }

    // Reads from STDIN as a string, accumulating lines into a
    // vector<string>.
    inline vector<string>* read_stdin()
    {
        auto output = new vector<string>;
        string line_buffer;
        while(std::getline(std::cin, line_buffer, '\n'))
        {
            output->push_back(to_upper(line_buffer));
        }
        return output;
    }

    // Reads from file as a string, accumulating lines into a
    // vector<string>.
    inline vector<string>* read_file(string filename)
    {
        auto output = new vector<string>;
        std::fstream file;
        file.open(filename, std::fstream::in);
        if (file.is_open())
        {
            string line_buffer;
            while(std::getline(file, line_buffer, '\n'))
            {
                output->push_back(to_upper(line_buffer));
            }
            file.close();
        }
        return output;
    }
}