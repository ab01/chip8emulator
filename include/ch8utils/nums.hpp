#pragma once

#include <string>
#include <cstdint>

namespace utils::num
{
    using std::string;

    // converts Hex string to uint16
    inline uint16_t hex_to_int(string s)
    {
        return (uint16_t)(std::stoi(s, nullptr, 16));
    }

    inline uint16_t bin_to_int(string s)
    {
        return (uint16_t)(std::stoi(s, nullptr, 2));
    }

    inline uint16_t decimal_to_int(string s)
    {
        return (uint16_t)(std::stoi(s, nullptr, 10));
    }

    // converts Decimal, Hex, or Binary integer string to uint16
    inline uint16_t string_to_int(string s)
    {
        //if not starting with prefix, assume decimal.
        if (s.size() < 3) goto decimal;
        // check if hex
        if (s.substr(0,2) == "0X")
            return hex_to_int(s.substr(2,s.size()-2));
        // check if binary
        if (s.substr(0,2) == "0B")
            return bin_to_int(s.substr(2,s.size()-2));

        // else assume decimal
        decimal:
        return decimal_to_int(s);
    }
}