#pragma once

#ifndef ANSI_COLOR_CODES
#define ANSI_COLOR_CODES
#define ANSI_CLEAR_COLOR "\u001b[0m"
#define ANSI_BLACK_FG "\u001b[30m"
#define ANSI_RED_FG "\u001b[31m"
#define ANSI_GREEN_FG "\u001b[32m"
#define ANSI_YELLOW_FG "\u001b[33m"
#define ANSI_BLUE_FG "\u001b[34m"
#define ANSI_MAGENTA_FG "\u001b[35m"
#define ANSI_CYAN_FG "\u001b[36m"
#define ANSI_WHITE_FG "\u001b[37m"

#define ANSI_BRIGHT_BLACK_FG "\u001b[90m"
#define ANSI_BRIGHT_RED_FG "\u001b[91m"
#define ANSI_BRIGHT_GREEN_FG "\u001b[92m"
#define ANSI_BRIGHT_YELLOW_FG "\u001b[93m"
#define ANSI_BRIGHT_BLUE_FG "\u001b[94m"
#define ANSI_BRIGHT_MAGENTA_FG "\u001b[95m"
#define ANSI_BRIGHT_CYAN_FG "\u001b[96m"
#define ANSI_BRIGHT_WHITE_FG "\u001b[97m"
#endif